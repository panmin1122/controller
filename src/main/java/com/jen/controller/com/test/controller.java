package com.jen.controller.com.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pm
 * @description
 * date 2022/10/13 17:40
 */
@RestController
public class controller {
    @GetMapping("/test")
    public String test(){
        return "Hello Jenkins!new";
    }
}
